<%@ page import="com.bc.webdoctor.sqlserverimporter.utils.GenderTypes"       %>
<%@ page import="com.bc.webdoctor.sqlserverimporter.model.sqlserver.WebUser" %>
<%@ page import="com.bc.webdoctor.sqlserverimporter.model.sqlserver.Patient" %>
<%@ page import="com.bc.webdoctor.sqlserverimporter.model.sqlserver.Doctor"  %>
<%@ page import="com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientContact"  %>

<h3>
<div
	class='alert alert-success'
	style='width:95%;text-align:left;font-size:29px;pad-right:300px;'
><liferay-ui:message key='GeneralInformation.pageHeader'/></div>
</h3>

<%
	logger.trace("===============> setting variables");

	WebUser theWebUser = theUser.getWebUserDetails();
	logger.trace("===============> variable 'theWebUser' set; {}", theWebUser);
	
	Patient thePatient = theUser.getPatientDetails();
	logger.trace("===============> variable 'thePatient' set; {}", thePatient);
	
	Doctor  theDoctor  = thePatient.getDoctor();
	logger.trace("===============> variable 'theDoctor' set; {}", theDoctor);
	
	PatientContact firstContact  =  thePatient.getFirstContact();
	logger.trace("===============> variable 'firstContact' set; {}", firstContact);
	
	PatientContact secondContact =  thePatient.getSecondContact();
	logger.trace("===============> variable 'secondContact' set; {}", secondContact);
%>

<c:set var='theUser'        value='<%= theUser %>'    />
<c:set var='theWebUser'     value='<%= theWebUser %>' />
<c:set var='thePatient'     value='<%= thePatient %>' />
<c:set var='theDoctor'      value='<%= theDoctor %>'  />
<c:set var='firstContact'   value='<%= firstContact %>'   />
<c:set var='secondContact'  value='<%= secondContact %>' />


<%-- c:set var='patientGender' value='${getGenderMessage(theWebUser.gender)}' / --%>







<%
	// questa SCRIPTLET � da rimuovere dopo i miei test
	//theUser.setLastName(null);
	logger.trace("===============> theUser.getLastName: {}", theUser.getLastName());
	logger.trace("===============> theWebUser.getFirstName: {}", theWebUser.getFirstName());
	
	String TO_BE_FILLED = "--- TO_BE_FILLED ---";
%>

<h4><liferay-ui:message key='GeneralInformation.personalData'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key="GeneralInformation.patientFirstName" /></strong></td>        <td>${theWebUser.firstName}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key="GeneralInformation.patientMiddleName" /></strong></td>       <td>${theWebUser.middleName}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key="GeneralInformation.patientLastName" /></strong></td>         <td>${theWebUser.lastName}</td></tr>
			<tr>
				<td style='width:25%;'><strong><liferay-ui:message key="GeneralInformation.patientDateOfBirth" /></strong></td>
				<td><fmt:formatDate value='${theWebUser.dateOfBirth}' pattern='dd/MMM/yyyy' /></td>
			</tr>
			<tr>
				<td style='width:25%;'><strong><liferay-ui:message key="GeneralInformation.patientGender" /></strong></td>
				<td>
					<c:choose>
						<c:when test='${theWebUser.male}'  ><liferay-ui:message key='GeneralInformation.patientGender.Male'/></c:when>
						<c:when test='${theWebUser.female}'><liferay-ui:message key='GeneralInformation.patientGender.Female'/></c:when>
						<c:otherwise>&nbsp;</c:otherwise>
					</c:choose>
				</td>
			</tr>

			<tr><td style='width:25%;'><strong><liferay-ui:message key="GeneralInformation.patientBloodType" /></strong></td>        <td>${thePatient.bloodType}${thePatient.rhBloodType}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.address'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientAddrStreet' /></strong></td>         <td>${theWebUser.street}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientAddrNumber' /></strong></td>         <td>${theWebUser.houseNumber}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientAddrCity' /></strong></td>           <td>${theWebUser.city}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientAddrZip' /></strong></td>            <td>${theWebUser.zipCode}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientAddrState' /></strong></td>          <td>${theWebUser.state}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientAddrCountry' /></strong></td>        <td>${theWebUser.country}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.telephoneAndMail'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientPhone.1stPhone' /></strong></td>        <td>${theWebUser.firstPhone.fullNumber}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientPhone.2ndPhone' /></strong></td>        <td>${theWebUser.secondPhone.fullNumber}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.patientPhone.email' /></strong></td>           <td>${theWebUser.emailAddress}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.remarks'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.remarksField' /></strong></td>           <td>${TO_BE_FILLED}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.insuranceCompany'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.healthInsuranceCompany.companyName' /></strong></td> <td>${thePatient.healthInsuranceCompany}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.healthInsuranceCompany.product' /></strong></td>     <td>${thePatient.healthInsuranceProduct}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.healthInsuranceCompany.policy' /></strong></td>      <td>${thePatient.healthInsurancePolicy}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.physician'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.physician.physicianName' /></strong></td>          <td>${theDoctor.fullName}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.physician.physicianPhone' /></strong></td>         <td>${theDoctor.phone.fullNumber}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.physician.physicianFax' /></strong></td>           <td>${theDoctor.fax.fullNumber}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.firstContact'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.contact.name' /></strong></td>           <td>${firstContact.fullName}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.contact.1stPhone' /></strong></td>       <td>${firstContact.firstPhone.fullNumber}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.contact.2ndPhone' /></strong></td>       <td>${firstContact.secondPhone.fullNumber}</td></tr>
		</table>
	</div>
</p>
<br>

<h4><liferay-ui:message key='GeneralInformation.secondContact'/></h4>
<p>
	<div style='padding-left:10px;'>
		<table style='width:100%;'>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.contact.name' /></strong></td>           <td>${secondContact.fullName}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.contact.1stPhone' /></strong></td>       <td>${secondContact.firstPhone.fullNumber}</td></tr>
			<tr><td style='width:25%;'><strong><liferay-ui:message key='GeneralInformation.contact.2ndPhone' /></strong></td>       <td>${secondContact.secondPhone.fullNumber}</td></tr>
		</table>
	</div>
</p>
<br>


