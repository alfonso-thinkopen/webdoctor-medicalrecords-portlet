<h3>
<div
	class='alert alert-success'
	style='width:95%;text-align:left;font-size:29px;'
><liferay-ui:message key='MedicalInformation.pageHeader'/></div>
</h3>

<jsp:include page='/html/medicalrecords/fragments/diseases.jsp' />
<jsp:include page='/html/medicalrecords/fragments/medications.jsp' />
<jsp:include page='/html/medicalrecords/fragments/hospitalizations.jsp' />
<jsp:include page='/html/medicalrecords/fragments/allergies.jsp' />
<jsp:include page='/html/medicalrecords/fragments/addictions.jsp' />
<jsp:include page='/html/medicalrecords/fragments/vaccinations.jsp' />
<jsp:include page='/html/medicalrecords/fragments/prosthesis.jsp' />
<jsp:include page='/html/medicalrecords/fragments/irregular-tests.jsp' />