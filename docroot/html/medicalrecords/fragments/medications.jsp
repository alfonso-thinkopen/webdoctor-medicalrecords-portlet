<%@ include file='/html/medicalrecords/init.jsp' %>

<%-- @ page import='com.bc.webdoctor.model.PatientMedication' --%>
<%@ page import='com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientMedication' %>

<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>


<%
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE");
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE");
	LoggedUser current_LOGGED_USER       = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	
	LoggedUser theUser = null;
	theUser = current_PORTLET_SCOPE;
	theUser = current_APPLICATION_SCOPE;
	theUser = current_LOGGED_USER;
	logger.debug("theUser: {}", theUser);
%>

<%-- =============================================================================================================

								FARMACI

============================================================================================================= --%>
<% logger.trace("processing PATIENT MEDICATIONS: start"); %>
<h3><liferay-ui:message key='Medication.sectionHeader'/></h3>
<c:choose>

	<c:when test="<%= theUser != null %>">
		<%
		List<PatientMedication> medications = theUser.getMedications();

		logger.debug("current user medications size: {}", medications.size());
		for(PatientMedication medication : medications ){
			logger.trace("allergy: {}", medication);
		}
		logger.trace("RESULTS.JSP: current user medications size: {}", medications.size());
		SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
		
		
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.setParameter("mvcPath", "/html/medicalrecords/results.jsp");
		
		
		
		
		
		%>
		
		<liferay-ui:search-container iteratorURL='<%= iteratorURL %>' emptyResultsMessage='Medication.NoMedicationsForThisUser' >

			<liferay-ui:search-container-results results='<%= medications.subList(searchContainer.getStart(), Math.min(searchContainer.getEnd(), medications.size())) %>' />

				<liferay-ui:search-iterator paginate='false'>
			

				<liferay-ui:search-container-row className='PatientMedication' modelVar='medication'>
				
					<liferay-ui:search-container-column-text name='Medication.Name'        value='${medication.getMedicineDisplayName()}' />
					<liferay-ui:search-container-column-text name='Medication.GenericName' value='${medication.getGenericName()}' />
					
					<liferay-ui:search-container-column-text name='Medication.SinceDate'>
						<fmt:formatDate value='${medication.getSince()}' pattern='yyyy' />
					</liferay-ui:search-container-column-text>
				
					<liferay-ui:search-container-column-text name="Medication.Dosage"          value='${medication.getDosage()}' />
					<liferay-ui:search-container-column-text name="Medication.MedicationForm"  value='${medication.getWayOfUsageName()}' />
					<liferay-ui:search-container-column-text name="Medication.Remarks"         value='${medication.getRemark()}' />
					
				</liferay-ui:search-container-row>



			</liferay-ui:search-iterator>
		</liferay-ui:search-container>

	</c:when>
	
	<c:otherwise>
		USER is NULL
	</c:otherwise>
	
</c:choose>




<br/><br/>
<% logger.trace("process PATIENT MEDICATIONS: done"); %>
<%-- ======================================================================================================== --%>

