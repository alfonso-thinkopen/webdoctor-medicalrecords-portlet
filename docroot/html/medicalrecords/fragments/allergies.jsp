<%@ include file='/html/medicalrecords/init.jsp' %>

<%@ page import='com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientAllergy' %>

<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>

<%
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE");
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE");
	LoggedUser current_LOGGED_USER       = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	LoggedUser theUser = null;
	theUser = current_PORTLET_SCOPE;
	theUser = current_APPLICATION_SCOPE;
	theUser = current_LOGGED_USER;
	logger.debug("theUser: {}", theUser);
%>




<%--
=============================================================================================================

										ALLERGIE

=============================================================================================================
--%>
<% logger.trace("processing ALLERGIES: start"); %>
<%-- @ include file="/html/medicalrecords/results_allergies.jsp" --%> 




<h3><liferay-ui:message key='Allergy.sectionHeader'/></h3>
<c:choose>

	<c:when test="<%= theUser != null %>">
		<%
		logger.trace("current user: {}", theUser);
	
		List<PatientAllergy> allergies = theUser.getAllergies();

		logger.debug("current user allergies size: {}", theUser.getAllergies().size());
		for(PatientAllergy allergy : theUser.getAllergies() ){
			logger.trace("allergy: {}", allergy);
		}
		logger.trace("RESULTS.JSP: current user allergies size: {}", theUser.getAllergies().size());
		SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
		
		
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.setParameter("mvcPath", "/html/medicalrecords/results.jsp");
		
		
		
		
		
		%>
		
		<liferay-ui:search-container iteratorURL="<%= iteratorURL %>" emptyResultsMessage="Allergy.NoAllergiesForThisUser">

			<liferay-ui:search-container-results results="<%= allergies.subList(searchContainer.getStart(), Math.min(searchContainer.getEnd(),allergies.size())) %>"/>

				<liferay-ui:search-iterator paginate="false">
			

				<liferay-ui:search-container-row className="PatientAllergy" modelVar="allergy">
				
					<liferay-ui:search-container-column-text name="Allergy.AllergyName" value="${allergy.getDisplayAllergyName()}" />
					<liferay-ui:search-container-column-text name="Allergy.GenericName" value="${allergy.getGenericName()}" />
						
					<liferay-ui:search-container-column-text name="Allergy.AllergyDate">
						<fmt:formatDate value="${allergy.getAllergyDate()}" pattern="yyyy" />
					</liferay-ui:search-container-column-text>
					
					<liferay-ui:search-container-column-text name="Allergy.Remarks" property="displayRemark" />


					<liferay-ui:search-container-column-text name="Allergy.ShowOnEmergencyCard">
						<aui:input name="" property="showOnEmergencyCard" type="checkbox"
							readonly="readonly" disabled="disabled" 
							value="<%= allergy.isShowOnEmergencyCard() %>" onClick="return false;"/>
					</liferay-ui:search-container-column-text>
					
				</liferay-ui:search-container-row>



			</liferay-ui:search-iterator>
		</liferay-ui:search-container>

	</c:when>
	
	<c:otherwise>
		USER is NULL
	</c:otherwise>
	
</c:choose>

<br/><br/>
<% logger.trace("processing ALLERGIES: done"); %>
<%-- ==== end: ALLERGIE====================================================================================== --%>

