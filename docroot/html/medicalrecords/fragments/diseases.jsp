<%@ include file='/html/medicalrecords/init.jsp' %>

<%@ page import='com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientDisease' %>

<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>

<%
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE");
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE");
	LoggedUser current_LOGGED_USER       = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	
	LoggedUser theUser = null;
	theUser = current_PORTLET_SCOPE;
	theUser = current_APPLICATION_SCOPE;
	theUser = current_LOGGED_USER;
	logger.debug("theUser: {}", theUser);
%>


<%-- =============================================================================================================

								MALATTIE

============================================================================================================= --%>
<% logger.trace("processing PATIENT DISEASES: start"); %>
<h3><liferay-ui:message key='Disease.sectionHeader'/></h3>
<c:choose>

	<c:when test="<%= theUser != null %>">
		<%
		List<PatientDisease> diseases = theUser.getDiseases();

		logger.debug("current user diseases size: {}", diseases.size());
		for(PatientDisease disease : diseases ){
			logger.trace("disease: {}", diseases);
		}
		logger.trace("RESULTS.JSP: current user diseases size: {}", diseases.size());
		SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
		
		
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.setParameter("mvcPath", "/html/medicalrecords/results.jsp");
		%>
		
		<liferay-ui:search-container iteratorURL='<%= iteratorURL %>' emptyResultsMessage='Disease.NoDiseasesForThisUser' >

			<liferay-ui:search-container-results results='<%= diseases.subList(searchContainer.getStart(), Math.min(searchContainer.getEnd(), diseases.size())) %>' />

				<liferay-ui:search-iterator paginate='false'>
			

				<liferay-ui:search-container-row className='PatientDisease' modelVar='disease'>
				
					<liferay-ui:search-container-column-text name='Disease.Name'          value='${disease.getDisplayName()}' />
					<liferay-ui:search-container-column-text name='Disease.CategoryName'  value='${disease.getDiseaseCategoryName()}' />
					
					<liferay-ui:search-container-column-text name='Disease.SinceDate'>
						<fmt:formatDate value='${disease.getDateDiscovered()}' pattern='yyyy' />
					</liferay-ui:search-container-column-text>
								
					<liferay-ui:search-container-column-text name='Disease.Complications' value='${disease.getComplications()}' />
					<liferay-ui:search-container-column-text name='Disease.Remarks'       value='${disease.getRemarks()}' />
					
				</liferay-ui:search-container-row>



			</liferay-ui:search-iterator>
		</liferay-ui:search-container>

	</c:when>
	
	<c:otherwise>
		USER is NULL
	</c:otherwise>
	
</c:choose>




<br/><br/>
<% logger.trace("processing PATIENT DISEASES: done"); %>
<%-- ======================================================================================================== --%>

