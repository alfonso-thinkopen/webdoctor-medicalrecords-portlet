<%@ include file='/html/medicalrecords/init.jsp' %>

<%@page import="com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientHabit"%>


<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>

<%
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE");
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE");
	LoggedUser current_LOGGED_USER       = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	LoggedUser theUser = null;
	theUser = current_PORTLET_SCOPE;
	theUser = current_APPLICATION_SCOPE;
	theUser = current_LOGGED_USER;
	logger.debug("theUser: {}", theUser);
%>

<%-- =============================================================================================================

								DIPENDENZE

============================================================================================================= --%>
<% logger.trace("processing PATIENT ADDICTIONS: start"); %>
<h3><liferay-ui:message key='Addiction.sectionHeader'/></h3>


<c:choose>

	<c:when test="<%= theUser != null %>">
		<%
		List<PatientHabit> habits = theUser.getHabits();

		logger.debug("current user habits size: {}", habits.size());
		for(PatientHabit habit : habits ){
			logger.trace("habit: {}", habit);
		}
		logger.trace("current user habits size: {}", habits.size());
		SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
		
		
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.setParameter("mvcPath", "/html/medicalrecords/results.jsp");
		%>
		
		<liferay-ui:search-container iteratorURL='<%= iteratorURL %>' emptyResultsMessage='Addiction.NoAddictionsForThisUser' >

			<liferay-ui:search-container-results results='<%= habits.subList(searchContainer.getStart(), Math.min(searchContainer.getEnd(), habits.size())) %>' />

				<liferay-ui:search-iterator paginate='false'>
			

				<liferay-ui:search-container-row className='PatientHabit' modelVar='habit'>
				
					<liferay-ui:search-container-column-text name='Addiction.Name'          value='${habit.getHabitDisplayName()}' />
					<liferay-ui:search-container-column-text name='Addiction.DailyUse'      value='${habit.getAmountPerDay()}' />
					<liferay-ui:search-container-column-text name='Addiction.StartDate'>
						<fmt:formatDate value='${habit.getStartDate()}' pattern='yyyy' />
					</liferay-ui:search-container-column-text>
					
					<liferay-ui:search-container-column-text name='Addiction.HasStopped'>
					<%-- liferay-ui:search-container-column-text name='Addiction.HasStopped'    value='' / --%>

					<c:choose>
						<c:when test='${habit.isOnGoing()}'><liferay-ui:message key='Common.Yes'/></c:when>
						<c:otherwise><liferay-ui:message key='Common.No'/></c:otherwise>
					</c:choose>
					</liferay-ui:search-container-column-text>
					
					<liferay-ui:search-container-column-text name='Addiction.EndDate'>
						<fmt:formatDate value='${habit.getEndDate()}' pattern='yyyy' />
					</liferay-ui:search-container-column-text>
					<liferay-ui:search-container-column-text name='Addiction.Remarks'       value='${habit.getRemarks()}' />
					
								
					
				</liferay-ui:search-container-row>



			</liferay-ui:search-iterator>
		</liferay-ui:search-container>

	</c:when>
	
	<c:otherwise>
		USER is NULL
	</c:otherwise>
	
</c:choose>



<br/><br/>
<% logger.trace("processing PATIENT ADDICTIONS: done"); %>
<%-- ======================================================================================================== --%>


