<%@ include file='/html/medicalrecords/init.jsp' %>
<%@ page import='com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientVaccination' %>
<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>

<%
	LoggedUser currUser = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	logger.debug("current user: {}", currUser);
%>





<%-- =============================================================================================================

								VACCINI

============================================================================================================= --%>
<% logger.trace("processing PATIENT VACCINATIONS: start"); %>
<h3><liferay-ui:message key='Immunization.sectionHeader'/></h3>
<c:choose>

	<c:when test="<%= currUser != null %>">
		<%
		List<PatientVaccination> vaccinations = currUser.getVaccinations();

		logger.debug("current user vaccinations size: {}", vaccinations.size());
		for(PatientVaccination pv : vaccinations ){
			logger.trace("vaccination: {}", pv);
		}
		logger.trace("vaccination.jsp: current user vaccinations size: {}", vaccinations.size());
		SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
		
		
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.setParameter("mvcPath", "/html/medicalrecords/results.jsp");
		%>
		
		<liferay-ui:search-container iteratorURL='<%= iteratorURL %>' emptyResultsMessage='Immunization.NoVaccinationsForThisUser' >

			<liferay-ui:search-container-results results='<%= vaccinations.subList(searchContainer.getStart(), Math.min(searchContainer.getEnd(), vaccinations.size())) %>' />

				<liferay-ui:search-iterator paginate='false'>
			

				<liferay-ui:search-container-row className='PatientVaccination' modelVar='vaccination'>
				
				
					<liferay-ui:search-container-column-text name='Immunization.VaccinationName' value='${vaccination.getDisplayName()}' />
					<liferay-ui:search-container-column-text name='Immunization.AdministeredDate'>
						<fmt:formatDate value='${vaccination.getAdministeredDate()}' pattern='dd/MM/yyyy' /><%-- TODO: what is GoodDate in report.asp????? --%>
					</liferay-ui:search-container-column-text>
					<liferay-ui:search-container-column-text name='Immunization.MethodName'      value='${vaccination.getVaccinMethodName()}' />
					<liferay-ui:search-container-column-text name='Immunization.Remarks'         value='${vaccination.getRemark()}' />
				
				
					
				</liferay-ui:search-container-row>



			</liferay-ui:search-iterator>
		</liferay-ui:search-container>

	</c:when>
	
	<c:otherwise>
		USER is NULL
	</c:otherwise>
	
</c:choose>




<br/><br/>
<% logger.trace("processing PATIENT VACCINATIONS: done"); %>
<%-- ======================================================================================================== --%>


