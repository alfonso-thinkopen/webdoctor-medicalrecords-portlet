<%@ include file='/html/medicalrecords/init.jsp' %>

<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>

<%
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE");
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE");
	LoggedUser current_LOGGED_USER       = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	LoggedUser theUser = null;
	theUser = current_PORTLET_SCOPE;
	theUser = current_APPLICATION_SCOPE;
	theUser = current_LOGGED_USER;
	logger.debug("theUser: {}", theUser);
%>



<%-- =============================================================================================================

								PROTESI/PRESIDIO

============================================================================================================= --%>
<% logger.trace("processing PATIENT S: start"); %>
<h3><liferay-ui:message key='Prosthesis.sectionHeader'/></h3>
<br/><br/>
<% logger.trace("processing PATIENT S: done"); %>
<%-- ======================================================================================================== --%>
