<%@ include file='/html/medicalrecords/init.jsp' %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import='com.bc.webdoctor.sqlserverimporter.model.sqlserver.LoggedUser' %>


<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");

	logger.trace("=== VIEW.JSP: START =================================================");
%>



<%!
	private static boolean isFreshSearch(PortletSession myPortSession)
	{
		if (myPortSession == null){
			return false;
		}
		boolean retValue = false;
		
		Object myAttribute = myPortSession.getAttribute(MedicalRecordsPortlet.IS_FRESH_SEARCH, PortletSession.APPLICATION_SCOPE);
		if (myAttribute == null) {
			return false;
		}
		return ((Boolean) myAttribute);
	}
%>



<%!
	private static boolean searchWasSuccessful(PortletSession myPortSession)
	{
		if (myPortSession == null){
			return false;
		}
		boolean retValue = false;
		
		Object myAttribute = myPortSession.getAttribute(MedicalRecordsPortlet.LOGGED_USER, PortletSession.APPLICATION_SCOPE);
		return (myAttribute != null);
	}
%>






<%
	PortletSession portSession = renderRequest.getPortletSession();
	/*
	LoggedUser current_NO_SCOPE          = (LoggedUser) portSession.getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_NO_SCOPE");
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) portSession.getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE",     PortletSession.PORTLET_SCOPE);
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) portSession.getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE", PortletSession.APPLICATION_SCOPE);
	*/
	LoggedUser current_user              = (LoggedUser) portSession.getAttribute(MedicalRecordsPortlet.LOGGED_USER, PortletSession.APPLICATION_SCOPE);
	
	LoggedUser theUser = current_user;
%>





<portlet:actionURL var="searchFormAction" name="getMedicalRecords" />


<aui:model-context bean="<%= theUser %>" model="<%= com.bc.webdoctor.sqlserverimporter.model.sqlserver.LoggedUser.class %>" >
	<aui:form name="<portlet:namespace/>searchQueryForm" action="<%= searchFormAction %>" label="search">
	
		<aui:fieldset>
			<aui:input name="userId" type="text" /> 
			<aui:input name="userPwd" type="text" label="MedicalRecords.userPwd" />
		</aui:fieldset>
		
		<aui:button-row>
			<aui:button type="submit" value="Search"></aui:button>
			<aui:button type="reset"  value="Reset"></aui:button>
		</aui:button-row>
	
	</aui:form>
</aui:model-context>



<%
	logger.trace("VIEW.jsp: portSession != null: {}", (portSession != null));
	logger.trace("VIEW.jsp: isFreshSearch != null: {}", portSession.getAttribute(MedicalRecordsPortlet.IS_FRESH_SEARCH, PortletSession.APPLICATION_SCOPE));
	
	logger.trace("VIEW.jsp: isFreshSearch: {}", isFreshSearch(portSession));
	logger.trace("VIEW.jsp: searchWasSuccessful: {}", searchWasSuccessful(portSession));
	
	boolean isFreshSearch = isFreshSearch(portSession);
	
	
	
	String errorKey = MedicalRecordsPortlet.MESSAGE_NO_USER_FOUND;
	String errorMessage = LanguageUtil.format(pageContext, "MedicalRecords.UserNotFound", renderRequest.getAttribute(MedicalRecordsPortlet.SEARCHED_USER_ID));
	
	logger.trace("key: {}; msg: {}", errorKey, errorMessage);
%>


<liferay-ui:error key='<%= errorKey %>' message='<%= errorMessage %>'/>


<c:if test="<%= !isFreshSearch(portSession) && theUser != null %>">
	<div id="successAlertBox" class="alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" onclick="$('#successAlertBox').close(); return false;">&times;</button>
			Sei loggato come <%= theUser.getFirstName() %> <%= theUser.getLastName() %>
	</div>
</c:if>


<%	
	logger.trace("=== VIEW.JSP: END ==================================================");
%>
