<%@ include file='/html/medicalrecords/init.jsp' %>
<%@ page import='com.liferay.portal.kernel.util.ParamUtil' %>

<%
	Logger logger = LogManager.getLogger("com.bc.webdoctor.portlet.MedicalRecordsPortlet.Jsp");
%>
<%
	logger.trace("=== RESULTS.JSP: START =================================================");
%>



<%
	LoggedUser current_PORTLET_SCOPE     = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_PORTLET_SCOPE");
	LoggedUser current_APPLICATION_SCOPE = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER+"_APPLICATION_SCOPE");
	LoggedUser current_LOGGED_USER       = (LoggedUser) request.getSession().getAttribute(MedicalRecordsPortlet.LOGGED_USER);
	
	
	LoggedUser theUser = null;
	theUser = current_PORTLET_SCOPE;
	theUser = current_APPLICATION_SCOPE;
	theUser = current_LOGGED_USER;
	logger.debug("theUser: {}", theUser);
%>
<c:set var='theUser' value='<%= theUser %>' /> 

<portlet:defineObjects />


<!-- <log:debug>=== RESULTS.JSP: START =================================================</log:debug> -->




<portlet:actionURL var="changeUser" name="changeUser" />


<aui:form action="<%= changeUser.toString() %>">
	<aui:button-row>
		<aui:button type="submit" value="MedicalRecords.SearchAnotherUser" />
	</aui:button-row>
</aui:form>



<!-- **************************************************************************************************************************** -->



<!-- **************************************************************************************************************************** -->
<%--
 VIEW THE SEARCH RESULTS
--%>


<%
	// Tab value to preselect on page load
	// Default parameter name used by the taglib is "tabs1"
	String currentTab = ParamUtil.getString(request, "tab", "tab.medical-info");
%>
<portlet:renderURL var='changeTabURL' />



<%-- liferay-ui:tabs
	names='tab.general-info,tab.medical-info'
	tabsValues='tab.general-info,tab.medical-info'
	
	url='<%= changeTabURL %>'
	param='tab'
>
	<liferay-ui:section><%@include file='/html/medicalrecords/tabs/general-information.jsp' %></liferay-ui:section>
	<liferay-ui:section><%@include file='/html/medicalrecords/tabs/medical-information.jsp' %></liferay-ui:section>
</liferay-ui:tabs --%>



<%@ include file='/html/medicalrecords/tabs/general-information.jsp' %>
<%@ include file='/html/medicalrecords/tabs/medical-information.jsp' %>






<%
	logger.trace("=== RESULTS.JSP: END ==================================================");
%>
