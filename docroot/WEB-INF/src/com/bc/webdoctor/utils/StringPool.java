package com.bc.webdoctor.utils;

public class StringPool {

	public static final String PASSWORD_OBSCURED = "<OBSCURED>";
	public static final String PHONE_NUMBER_SEPARATOR = "-";
}
