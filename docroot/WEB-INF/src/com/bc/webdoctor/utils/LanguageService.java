package com.bc.webdoctor.utils;

import java.util.ArrayList;


public class LanguageService {

	static final ArrayList<String> LanguageMap = new ArrayList<String>();

	static {
		LanguageMap.add("en"); // English
		LanguageMap.add("it"); // Italian
		LanguageMap.add("de"); // German
		LanguageMap.add("es"); // Spanish
		LanguageMap.add("fr"); // French
		LanguageMap.add("cn"); // Simplified Chinese
		LanguageMap.add("th"); // Thai
		LanguageMap.add("pt"); // Portuguese
		LanguageMap.add("il"); // Jewish
		LanguageMap.add("ru"); // Russian
		LanguageMap.add("tr"); // Turkish
		LanguageMap.add("gr"); // Greek
		LanguageMap.add("cz"); // Czech
		LanguageMap.add("xx"); // Traditional Chinese // TODO verify what is the right code
		LanguageMap.add("hu"); // Hungarian
		LanguageMap.add("ro"); // Romanian
		LanguageMap.add("pl"); // Polish
		LanguageMap.add("nl"); // Dutch
		LanguageMap.add("fi"); // Finnish
	}
	
	
	
	static public int getLanguageCode(String aLanguage)
	{
		return LanguageMap.indexOf(aLanguage)+1;
	}

	
	static public String getLanguage(int languageCode)
	{
		return LanguageMap.get(languageCode);
	}
	
	
	static public void main(String[] args)
	{
		System.out.printf("getLanguageCode(): %s\n", getLanguageCode("en"));
		System.out.printf("getLanguageCode(): %s\n", getLanguageCode("nl"));
		System.out.printf("getLanguageCode(): %s\n", getLanguageCode("zz"));
		System.out.printf("getLanguageCode(): %s\n", getLanguageCode("fi"));
	}
}
