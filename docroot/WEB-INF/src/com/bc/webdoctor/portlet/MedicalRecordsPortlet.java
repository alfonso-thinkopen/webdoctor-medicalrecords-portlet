package com.bc.webdoctor.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.util.bridges.mvc.MVCPortlet;

import com.bc.webdoctor.sqlserverimporter.database.DatabaseFacade;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.LoggedUser;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.Patient;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientAllergy;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientDisease;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientHabit;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientVaccination;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.WebUser;
import com.bc.webdoctor.sqlserverimporter.model.sqlserver.PatientMedication;
import com.bc.webdoctor.sqlserverimporter.utils.StringPool;

import com.bc.webdoctor.utils.LanguageService;

/**
 * Portlet implementation class MedicalRecordsPortlet
 */



// TODO [ ] usa log4j 2 per loggare anzich� i println
// TODO move constant strings to string pool
// TODO what to use to log exceptions
// TODO don't always log to ROOT LOGGER


public class MedicalRecordsPortlet extends MVCPortlet
{

	static public final String LOGGED_USER      = "logged_user";
	static public final String IS_FRESH_SEARCH  = "is_fresh_search";
		
	static public final String SEARCH_RESULT    = "search_result";
	static public final String SEARCH_FAILED    = "search_failed";
	static public final String SEARCH_SUCCEDED  = "search_succeded";
	static public final String SEARCHED_USER_ID = "searched_user_id";
	
	static public final String MESSAGE_NO_USER_FOUND = "message_no_user_found";
	
	private static final Logger logger = LogManager.getLogger();
	
	
	
	
	int run = 0;
	boolean isFreshSearch = true;
	
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException
	{
		logger.entry();
		run++;
		logger.debug("this is render #{}.", run);

		logger.exit();
		super.render(renderRequest, renderResponse);
	}

	
	// ======================================================================================================
	// this method is the form action
	//
	public static void changeUser(ActionRequest request, ActionResponse response)
	{
		logger.entry();
		
		PortletSession portSession = request.getPortletSession();
		portSession.removeAttribute(LOGGED_USER, PortletSession.APPLICATION_SCOPE);
		portSession.setAttribute(IS_FRESH_SEARCH, true, PortletSession.APPLICATION_SCOPE);
		
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		logger.exit();
	}
	//-------------------------------------------------------------------------------------------------------
	
	
	
	
	// ======================================================================================================
	// this method is the form action
	//
	public static void getMedicalRecords(ActionRequest request, ActionResponse response)
	{
		logger.entry();
		String paramUserId = request.getParameter("userId");
		String paramPasswd = request.getParameter("userPwd");
		
		logger.trace("getMedicalRecords -> paramUserId: {}", paramUserId);
		logger.trace("getMedicalRecords -> paramPasswd: {}", StringPool.PASSWORD_OBSCURED);
		
		PortletSession portSession = request.getPortletSession();
		portSession.setAttribute(IS_FRESH_SEARCH, false, PortletSession.APPLICATION_SCOPE);
		
		LoggedUser currentUser = getLoggedUser(request, response);
		if (currentUser == null)
		{
			portSession.removeAttribute(LOGGED_USER, PortletSession.APPLICATION_SCOPE);
			portSession.setAttribute(SEARCH_RESULT, SEARCH_FAILED, PortletSession.APPLICATION_SCOPE);
			
			request.setAttribute(SEARCHED_USER_ID, paramUserId);
			SessionErrors.add(request, MESSAGE_NO_USER_FOUND);

			logger.exit();
			return;
		}
		else {
			
			currentUser.setWebUserDetails(getWebUserDetails(request, response));
			currentUser.setPatientDetails(getPatientDetails(request, response));
			
			currentUser.setDiseases(getUserDiseases(request, response));
			currentUser.setAllergies(getUserAllergies(request, response));
			currentUser.setHabits(getUserHabits(request, response));
			currentUser.setMedications(getUserMedications(request, response));
			currentUser.setVaccinations(getUserVaccinations(request, response));
			
			logger.debug("currentUser: {}", currentUser);
			

			portSession.setAttribute(LOGGED_USER, currentUser, PortletSession.APPLICATION_SCOPE);
			portSession.setAttribute(SEARCH_RESULT, SEARCH_SUCCEDED, PortletSession.APPLICATION_SCOPE);
			
			// if a user is found in the database go to 'results.jsp' to show the relevant info
			response.setRenderParameter("jspPage", "/html/medicalrecords/results.jsp");
			logger.exit();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	
	
	
	
	

	

	// ======================================================================================================
	//
	private static LoggedUser getLoggedUser(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getLoggedUser");
		
		DatabaseFacade database = new DatabaseFacade();
		LoggedUser myLoggedUser = database.getLoggedUser(
				request.getParameter("userId"),
				request.getParameter("userPwd"));
		
		logger.trace("logged user: {}", myLoggedUser);
		logger.exit("exiting getLoggedUser");
		return myLoggedUser;
	}
	//-------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	// ======================================================================================================
	//
	private static WebUser getWebUserDetails(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getWebUserDetails(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		WebUser wu = database.getWebUserDetails(request.getParameter("userId"));
		
		logger.trace("web user: {}", wu);
		logger.trace("exiting getWebUserDetails(...)");
		return wu;
	}
	//-------------------------------------------------------------------------------------------------------
	
	

	// ======================================================================================================
	//
	private static Patient getPatientDetails(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getPatientDetails(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		Patient pat = database.getPatientDetails(request.getParameter("userId"), getLangCode(request));
		
		logger.trace("patient: {}", pat);
		logger.exit("exiting getPatientDetails(...)");
		return pat;
	}
	//-------------------------------------------------------------------------------------------------------
	


	// ======================================================================================================
	//
	private static List<PatientAllergy> getUserAllergies(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getUserAllergies(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		List<PatientAllergy> allergies = database.getPatientAllergies(
				request.getParameter("userId"),
				getLangCode(request));
		
		logger.trace("allergies: {}", allergies);
		logger.exit("exiting getUserAllergies(...)");
		return allergies;
	}
	//-------------------------------------------------------------------------------------------------------
	
	
	

	// ======================================================================================================
	//
	private static List<PatientDisease> getUserDiseases(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getUserDiseases(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		List<PatientDisease> diseases = database.getPatientDiseases(
				request.getParameter("userId"),
				getLangCode(request));
		
		logger.trace("diseases: {}", diseases);
		logger.exit("exiting getUserDiseases(...)");
		return diseases;
	}
	//-------------------------------------------------------------------------------------------------------
	
	
	
	
	
	// ======================================================================================================
	//
	private static List<PatientHabit> getUserHabits(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getUserHabits(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		List<PatientHabit> habits = database.getPatientHabits(
				request.getParameter("userId"),
				getLangCode(request));
		
		logger.trace("habits: {}", habits);
		logger.exit("exiting getUserHabits(...)");
		return habits;
	}
	//-------------------------------------------------------------------------------------------------------
	
	

	

	// ======================================================================================================
	//
	private static List<PatientMedication> getUserMedications(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getUserMedications(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		List<PatientMedication> medications = database.getPatientMedications(
				request.getParameter("userId"),
				getLangCode(request));
		
		logger.trace("medications: {}", medications);
		logger.exit("exiting getUserMedications(...)");
		return medications;
	}
	//-------------------------------------------------------------------------------------------------------


	
	

	

	// ======================================================================================================
	//
	private static List<PatientVaccination> getUserVaccinations(ActionRequest request, ActionResponse response)
	{
		logger.entry("entering getUserVaccinations(...)");
		
		DatabaseFacade database = new DatabaseFacade();
		List<PatientVaccination> vaccinations = database.getPatientVaccinations(
				request.getParameter("userId"),
				getLangCode(request));
		
		logger.trace("vaccinations: {}", vaccinations);
		logger.exit("exiting getUserVaccinations(...)");
		return vaccinations;
	}
	//-------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//-------------------------------------------------------------------------------------------------------
	private static int getLangCode(ActionRequest request) {
		int languageCode = LanguageService.getLanguageCode(LanguageUtil.getLanguageId(request).substring(0, 2));
		return languageCode;
	}
	//-------------------------------------------------------------------------------------------------------
}
